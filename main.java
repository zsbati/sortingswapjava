mport java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        int c = 0;
        int d = 0;
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        // Write Your Code Here
        int numberOfSwaps = 0;
        
        for (int i = 0; i < n; i++) {
        // Track number of elements swapped during a single array traversal
        
    
        for (int j = 0; j < n - 1; j++) {
         // Swap adjacent elements if they are in decreasing order
          if (a[j] > a[j + 1] && j + 1 <= n-1) {
            
            c = a[j];
            d = a[j+1];
            a[j] = d;
            a[j+1] = c;
        
            numberOfSwaps++;
        }
    }
    
}
    System.out.println("Array is sorted in "+numberOfSwaps+" swaps.");
    System.out.println("First Element: "+a[0]);
    System.out.println("Last Element: "+a[n-1]);
    }
    
    
    
}
